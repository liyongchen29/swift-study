//
//  LandmarksApp.swift
//  Landmarks
//
//  Created by Leo on 30/11/2022.
//

import SwiftUI

@main
struct LandmarksApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
