//
//  ContentView.swift
//  Landmarks
//
//  Created by Leo on 30/11/2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            VStack {
                Text("Turtle Rock")
                    .font(.title)
                    .fontWeight(.bold)
                Text("This is my first Swift project")
                .foregroundColor(Color.green)
            }
                
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
